 <%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>

<portlet:defineObjects/>



<style>

	body {
		margin-top: 40px;
		text-align: center;
		font-size: 14px;
		font-family: "Lucida Grande",Helvetica,Arial,Verdana,sans-serif;
		}
		
	#loading {
		position: absolute;
		top: 5px;
		right: 5px;
		}

	#calendar {
		width: 100%;
		margin: 0 auto;
		}

</style>
</head>
<body>
       <div class="dialog-modal" id="dialog-login" style="display: none;">
            U?ivatelsk� jm�no <br />
            <input type="text" id="usernameLogin" name="user" /><br />
            <button class="login" onclick="login()">P?ihl�sit</button>
        </div>
        <div class="dialog-modal" id="dialog-form" style="display: none;">
        Nab�dka akc�:
                <button class="showAlternatives">Zobrazit alternativn� hodiny</button>
                <button class="leave permanent">Odhl�sit se ze cvi?en� - permanentn?</button><br />
                <button class="leave one">Odhl�sit se ze cvi?en� - jednor�zov?</button>
                <button class="hidden close" onclick="cancel()">Zru?it</button>
        </div>
        <div class="dialog-modal" id="dialog-join" style="display: none;">
        Nab�dka akc�:
                <button class="join permanent">P?ihl�sit se na jin� cvi?en� - permanentn?</button><br />
                <button class="join one">P?ihl�sit se na jin� cvi?en� - jednor�zov?</button>
                <button class="hidden close" onclick="cancel()">Zru?it</button>
        </div>
        <div class="dialog-modal" style="display: none;">
            P?ejete si dannou akci vykonat?
        </div>
        <div id="dialog-locked" style="display: none;">
            Dann� cvi?en� nen� mo?n� m?nit.
        </div>
        <div id="dialog-demand" style="display: none;">
            Dann� v�m?na byla odesl�na ke schv�len�.
        </div>
        <div id="dialog-results" style="display: none;">
            Status:<p class="status"></p>
            V�sledek:<p class="result"></p>
        </div>
        <div id='loading' style='display:none'>loading...</div>
        <div>
            U?ivatel <span id="username"></span>
        </div>
        <div>
            <button class="hidden close" onclick="cancel()">Zru?it</button>
        </div>
        <div id='calendar'></div>