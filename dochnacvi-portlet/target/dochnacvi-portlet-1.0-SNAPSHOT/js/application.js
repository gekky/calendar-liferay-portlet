/* 
 * Copyright (C) 2014 jonny.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 */


var domain = "http://localhost:8090/Exchange/";
var LoggedIn = false;
var EventsData = [];
var EventsSource = [];
var Exchanges = [];
var Action = null;
var SelectedEvents = {first:null,second:null};
var CurrentEvent = null;
var User = {name: null,id: null};
var UserExchage = {my: null,wanted: null,type: false};
/*
 * Events by course
 */
var SortedEvents = [];
var Courses = [];
var LoadedEvents = [];
var EventCallback = null;


/*
 * 
 * Click Actions on events
 */        
        
function availableClick(){
    $('#dialog-form').dialog({
        height: 400,
        width: 300,
        modal:true
    });
    OpenedDialogs.push($('#dialog-form'));
}

function demandClick(){
    $('#dialog-form').dialog({
        height: 400,
        width: 300,
        modal:true
    });
    OpenedDialogs.push($('#dialog-form'));
}



function lockedClick(){
    $('#dialog-locked').dialog({
        height: 400,
        width: 300,
        modal:true
    });
}

function joinClick(){
    $('#dialog-join').dialog({
        height: 400,
        width: 300,
        modal:true
    });
}


/*
 * Controllers mapping
 */


function lessonsByCourse(id){
    var script = document.createElement('script');
    script.src = domain + "timetable/" + "lessonsByCourse/" + id + ".json?callback=courseEvents";
    document.body.appendChild(script);
    JoinState = true;
    $('#dialog-form').dialog('close');
}

function actionRouter(evt){
    if(Action != null){
        switch(Action){
            case "cancel":
                cancel();
                break;
            case "lessonsByCourse":
                lessonsByCourse(evt.id);
                break;
            case "permanentExchange":
                
                break;
            case "oneExchange":
                
                break;
            case "leaveOne":
                setConfirm(leave,[CurrentEvent.id,false],"Jednorázové odhlášení");
                confirmDialog('leave');
                break;
            case "leavePermanent":
                setConfirm(leave,[CurrentEvent.id,true],"Jednorázová výměna");
                confirmDialog('leavePermanent');
                break;
            case "joinOne":
                EventsExchange.type = false;
                UserExchage.my = CurrentEvent;
                setConfirm(join,null,"Jednorázová výměna");
                confirmDialog();
                break;
            case "joinPermanent":
                UserExchage.type = true;
                UserExchage.my = CurrentEvent;
                setConfirm(join,null,"Permanentní výměna");
                confirmDialog();
                break;
            case "showAlternatives":
                lessonsByCourse(CurrentEvent.id);
                break;
        }
    }
}

function processData(data){
    $('.status').text(data['status']);
    $('.result').text(data['result']);
    $('#dialog-results').dialog({
        autoOpen: false,
        height: 600,
        width: 500,
        modal:true
    });
}

function showExchanges(){
    JSONP_puller("exchange","showExchanges",null,"callbackShowExchanges");
}

function exchange(event){
    
}

function callbackShowExchanges(data){
    $(".clearExchangeContent").text("");
    Exchanges = data;
    console.log(data);
    for(var i=0; i < Exchanges.length; i++){
        renderExchange(data[i]);
    }
}

function findDate(evt){
    $('#calendar').fullCalendar('gotoDate',evt.start);
    evt.className += " selected";
    $('#calendar').fullCalendar( 'renderEvent', evt );
}


function renderExchange(exchange,permanent){
    
    var xchg = "<a onclick=\"findDate("+exchange.id+")\" >"+exchange.sold.start.toString() + " " + exchange.bought.end.toString() + " "+exchange.sold.title+"</a>";
    if(permanent){
        $('#showExchanges.permanentExchange.clearExchangeContent').append($(xchg));
    }
    else{
        $('#showExchanges.oneExchange.clearExchangeContent').append($(xchg));
    }
    
}


function setConfirm(fn,args,message){
    $('#dialog-confirm').data('fn',fn);
    $('#dialog-confirm p').text(message);
    $('#dialog-confirm').data('args',args);
}

function confirmDialog(){
    $( "#dialog-confirm" ).dialog({
        resizable: false,
        height:140,
        modal: true,
        buttons: {
            "Potvrdit": function() {
                OpenedDialogs.remove($(this));
                $(this).data('fn');
                $(this).data('args');
                $( this ).dialog( "close" );
            },
            "Zrušit": function() {
                $(this) . data('fn' , 'cancel');
                $( this ).dialog( "close" );
                
            }
        }
    });
   
}
function showFilter(){
    $('#course-filters').empty();
    var ks = Object.keys(SortedEvents);
    for(var i=0;i < ks.length; i ++){
        var s = document.createElement('span');
        $(s).text(ks[i]);
        var e = document.createElement('input');
        e.type = "checkbox";
        e.value = "" + ks[i];
        $(e).click(function(){filterByCourse(this);}); 
        $(e).text(ks[i]);
        $(s).append(e);
        $('#course-filters').append(s);
    }
    $('#course-filters').show();
}

function filterByCourse(evt){
    EventsSource = SortedEvents[evt.value];
    $('#calendar').fullCalendar( 'refetchEvents' );
    $('#calendar').fullCalendar( 'rerenderEvents' );
}


function JSONP_puller(controller,action,params,callback){
    var script = document.createElement("script");
    var urlparams = "";
    if(params !== null){
        for(var i = 0;i < params.length; i+=2){
            if(params[i+1]  === undefined){
                urlparams += "&" + params[i];
            }
            else{
                urlparams += "&" + params[i] + "=" + params[i+1];
            }
        }
    }
    script.src = domain + controller + "/" + action + ".json?callback="+ callback + urlparams;
    document.body.appendChild(script);
}


/**
 * Fill global EventsData from JSONP source.
 * Done this according to cross  
 */
function parseEvents(data){
  EventsSource = data;
  for(var i=0; i < EventsSource.length; i++){
    EventsSource[i].url = null;
    EventsData.push(EventsSource[i]);
    if(SortedEvents[EventsSource[i].course] == null){
        SortedEvents[EventsSource[i].course] = [];
    }
    SortedEvents[EventsSource[i].course].push(EventsSource[i]);
  }
  EventsSource = EventsData;
  showFilter();
  EventCallback(EventsSource);
}


function login(){
    if(User.name == null){
        User.name = $('#usernameLogin').val();
        $('#dialog-login').dialog('close');
    }
    JSONP_puller("auth","restLogin",['user',User.name],"loginCredentials");
    
    //interval to stay logged with session
    
}

function loadEvents(start_date,end_date){
    var start = $.fullCalendar.moment(start_date);
    var end = $.fullCalendar.moment(end_date);
    /*if(LoadedEvents[start_date+end_date] == null)
    //var view = $('#calendar').fullCalendar( 'getView' );
    //JSONP_puller("timetable","showTimetable",["start",view.start.toLocaleString(),"end",view.end.toLocaleString()],"parseEvents");*/
    for(var i= start.dayOfYear();i <= end.dayOfYear(); i++){
        if(LoadedEvents[i] == null){
            start.add(1,'day');
        }
        else{
            break;
        }
    }
    if(start.dayOfYear() <= end.dayOfYear()){
        JSONP_puller("timetable","showTimetable",["start",start.toLocaleString(),"end",end.toLocaleString()],"parseEvents");
        for(var i=start.dayOfYear(); i <= end.dayOfYear(); i++){
            LoadedEvents[i] = true;
        }
    }
}

function loginCredentials(user){
    if(user !== null){
        if(User.name != null){
            setInterval(function (){login();}, 1200000);
        }
        User.name = user.name;
        User.id = user.id;
        window.localStorage['username'] = User.name;
        window.localStorage['user_id'] = User.id;
        if(EventsData.length <= 0){
            var view = $('#calendar').fullCalendar( 'getView' );
            loadEvents(view.start,view.end);
        }
        //interval to stay logged with session

        $("#username").text(User.name);
    }
    
}

function leave(id,permanent){
    var perm = permanent ? "1" : "0";
    JSONP_puller("exchange","leave",["id",id,"type",perm],"leaveCallback");
}

function join(){
    var perm = Exchanges ? "1" : "0";
    JSONP_puller("exhange","join",["id"])
}

function leaveCallback(data){
    var result = data["result"];
    var status = data["status"];
    var exchangeId = data["exchangeIds"];
    var permanent = data["permanent"];
    console.log(data);
    
}
        
function courseEvents(data){
    EventsSource = data;
    $('#calendar').fullCalendar( 'refetchEvents' );
    $('#calendar').fullCalendar( 'rerenderEvents' );
    $('.close').addClass('show');
    $('.close').removeClass('hidden');
}
        
function cancel(){
    EventsSource = EventsData;
    $('#calendar').fullCalendar( 'refetchEvents' );
    $('#calendar').fullCalendar( 'rerenderEvents' );
    $('.close').addClass('hidden');
    $('.close').removeClass('show');
    $('.dialog-confirm').dialog('close');
    CurrentEvent = null;
    Action = null;
    $(".ui-dialog-content").dialog("close");
}

function loginDialog(){
    $('#dialog-login').dialog({
        height: 400,
        width: 300,
        modal : true,
        close: function( event, ui ) {
            /*
                if(User.username == null){
                    $() . open();
                }
            */
            }    
    });
    $('#usernameLogin').keypress(function(e) {
    if(e.which == 13) {
        login();
    }
});
}

$(document).ready(function() {
        $("#loadingScreen").dialog({
        autoOpen: false,    // set this to false so we can manually open it
        dialogClass: "loadingScreenWindow",
        closeOnEscape: false,
        draggable: false,
        width: 460,
        minHeight: 50,
        modal: true,
        buttons: {},
        resizable: false,
        open: function() {
            // scrollbar fix for IE
            $('body').css('overflow','hidden');
        },
        close: function() {
            // reset overflow
            $('body').css('overflow','auto');
        }
    }); // end of dialog
    
    $('.leave.permanent').click(function(){
        Action = "leavePermanent";
    });
    $('.leave.one').click(function(){
        Action = "leaveOne";
    });
    $('.join.one').click(function(){
        Action = "joinOne";
    });
    $('.join.permanent').click(function(){
        Action = "joinPermanent";
        
    });
    $('.showAlternatives').click(function (){
        lessonsByCourse(CurrentEvent.id);
    });
    
    
/**
 *  Prototype for get week number recognize. 
 * @param {type} Date
 * @returns {Number} number of week
 */
	Date.prototype.getWeek = function (dowOffset) {
	/*getWeek() was developed by Nick Baicoianu at MeanFreePath: http://www.epoch-calendar.com */

		dowOffset = typeof(dowOffset) == 'int' ? dowOffset : 0; //default dowOffset to zero
		var newYear = new Date(this.getFullYear(),0,1);
		var day = newYear.getDay() - dowOffset; //the day of week the year begins on
		day = (day >= 0 ? day : day + 7);
		var daynum = Math.floor((this.getTime() - newYear.getTime() - 
		(this.getTimezoneOffset()-newYear.getTimezoneOffset())*60000)/86400000) + 1;
		var weeknum;
		//if the year starts before the middle of a week
		if(day < 4) {
			weeknum = Math.floor((daynum+day-1)/7) + 1;
			if(weeknum > 52) {
				nYear = new Date(this.getFullYear() + 1,0,1);
				nday = nYear.getDay() - dowOffset;
				nday = nday >= 0 ? nday : nday + 7;
				/*if the next year starts before the middle of
				  the week, it is week #1 of that year*/
				weeknum = nday < 4 ? 1 : 53;
			}
		}
		else {
			weeknum = Math.floor((daynum+day-1)/7);
		}
		return weeknum;
	};
	
	
	
	/**
         * Inicialization of callendar.
         * 
         */
         $('#calendar').fullCalendar({
                header: { left: "", center: "", right: "prev, today, next" },
                columnFormat: { week: 'ddd' },
                timeFormat: 'HH:mm',
                header: {
                    left: 'prev,next today',
                    center: 'title',
                    right: 'month,agendaWeek,agendaDay'
                },
                isRTL:false,
                minTime: '06:00:00',
                maxTime: '22:00:00',
                lang: 'cs',
                defaultView: 'agendaWeek',
                defaultDate: new Date(),
                editable: false,
                weekends: true,
                eventLimit: true,
                buttonText:{
                    today:    'dnes',
                    month:    'měsíc',
                    week:     'týden',
                    day:      'den'
                },
                firstDay: 1,
                eventClick: function(calEvent, jsEvent, view) {
                            /*
                            alert('Event: ' + calEvent.title);
                            alert('Coordinates: ' + jsEvent.pageX + ',' + jsEvent.pageY);
                            alert('View: ' + view.name);
                            */
                           CurrentEvent = calEvent;
                            console.log(jsEvent);
                            actionRouter(calEvent);
                            /*
                            if(JoinState){
                                joinClick();
                            }
                            else{
                                    switch (calEvent.eventLock){
                                    case "locked":
                                        lockedClick();
                                        break;
                                    case "demand":
                                        demandClick();
                                        break;
                                    case "unlocked":
                                        availableClick();
                                        break;
                                }
                            }
                            */
                            // change the border color just for fun
                            //$(this).css('border-color', 'red');

                        },
                events: function(start, end,timezone, callback) {
                    EventCallback = callback;
                    loadEvents(start,end);
                    console.log('calling');
                },
                loading: function( isLoading, view ){
                    if(isLoading){
                        $("#loadingScreen").dialog('open');
                    }
                    else{
                        $("#loadingScreen").dialog('close');
                    }
                }
            });
            // create the loading window and set autoOpen to false
    
    if(window.localStorage['username'] != null){
        User.name = window.localStorage['username'];
        User.id = window.localStorage['user_id'];
        login();
        $("#username").text(User.name);
    }
    else{
        loginDialog();
    }
});

